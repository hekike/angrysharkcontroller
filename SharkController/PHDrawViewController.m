//
//  PHDrawViewController.m
//  SharkController
//
//  Created by Peter Marton on 1/18/13.
//  Copyright (c) 2013 Peter Marton. All rights reserved.
//

#import "PHDrawViewController.h"

@interface PHDrawViewController ()

@end

@implementation PHDrawViewController {
    CGPoint lastPoint;
	UIImageView *drawImage;
	BOOL mouseSwiped;
	int mouseMoved;
    
    int startIndexX, startIndexY, endIndexX, endIndexY;
    double startTime;
    
    CGPoint startPoint;
    
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
	drawImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"44x44"]];
    drawImage.frame = self.view.frame;
	[self.view addSubview:drawImage];
	self.view.backgroundColor = [UIColor lightGrayColor];
	mouseMoved = 0;

    [self initNetworkCommunication];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	mouseSwiped = NO;
	UITouch *touch = [touches anyObject];
	
	if ([touch tapCount] == 2) {
		drawImage.image = nil;
		return;
	}
    
	lastPoint = [touch locationInView:self.view];
    startPoint = lastPoint;
    startTime = CFAbsoluteTimeGetCurrent();
	lastPoint.y -= 20;
    
    startIndexX = (int)(lastPoint.x - 12) / 44;
    startIndexY = (int)lastPoint.y / 44;
    // NSLog(@"begin: x: %d, y: %d", startIndexX, startIndexY);
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
	mouseSwiped = YES;
	
	UITouch *touch = [touches anyObject];
	CGPoint currentPoint = [touch locationInView:self.view];
	currentPoint.y -= 20;
	
	
	UIGraphicsBeginImageContext(self.view.frame.size);
	[drawImage.image drawInRect:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
	CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
	CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 5.0);
	CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 1.0, 0.0, 0.0, 1.0);
	CGContextBeginPath(UIGraphicsGetCurrentContext());
	CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
	CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
	CGContextStrokePath(UIGraphicsGetCurrentContext());
	drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	lastPoint = currentPoint;
    
	mouseMoved++;
	
	if (mouseMoved == 10) {
		mouseMoved = 0;
	}
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
	
	UITouch *touch = [touches anyObject];
	
	if ([touch tapCount] == 2) {
		drawImage.image = nil;
		return;
	}
    
    endIndexX = (int)(lastPoint.x - 12) / 44;
    endIndexY = (int)lastPoint.y / 44;
    // NSLog(@"end: x: %d, y: %d", endIndexX, endIndexY);
    
    float s = sqrt(pow((lastPoint.x - startPoint.x), 2) + pow((lastPoint.y - startPoint.y), 2));
    // NSLog(@"vec length: %f", s);
    double lengthTime = CFAbsoluteTimeGetCurrent() - startTime;
    
    //NSLog(@"time length: %f", lengthTime);
    
    float v = s / lengthTime;
   // NSLog(@"v: %f", v);
    
    float tanAlpha = (lastPoint.y - startPoint.y) / (lastPoint.x - startPoint.x);
    float angle = atan(tanAlpha);
    angle = angle * (180 / M_PI);
    
    if(angle < 0) {
        angle = 90 + angle;
    } else {
        angle =  -90 + angle;
    }
    
    NSLog(@"s: %f, v:%f, angle: %f", s, v, angle);
    [self sendMessage:[NSString stringWithFormat:@"%d,%d", (int)v, (int)angle]];
    
    
    [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(clearCanvas:) userInfo:nil repeats:NO];
	
	
	if(!mouseSwiped) {
		UIGraphicsBeginImageContext(self.view.frame.size);
		[drawImage.image drawInRect:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
		CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
		CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 5.0);
		CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 1.0, 0.0, 0.0, 1.0);
		CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
		CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
		CGContextStrokePath(UIGraphicsGetCurrentContext());
		CGContextFlush(UIGraphicsGetCurrentContext());
		drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
        
        // draw short direction
	}
    
}

- (void) clearCanvas:(NSTimer*)timer {
    drawImage.image = [UIImage imageNamed:@"44x44"];
}


# pragma mark communication

// initNetworkCommunication
- (void) initNetworkCommunication {
	
	CFReadStreamRef readStream;
	CFWriteStreamRef writeStream;
	CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"10.0.2.20", 6767, &readStream, &writeStream);
	
	inputStream = (NSInputStream *)CFBridgingRelease(readStream);
	outputStream = (NSOutputStream *)CFBridgingRelease(writeStream);
	[inputStream setDelegate:(id)self];
	[outputStream setDelegate:(id)self];
	[inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[inputStream open];
	[outputStream open];
    
	
}

// sendMessage
- (void)sendMessage:(NSString*) msg {
    NSLog(@"%@", msg);
    
    NSString *response  = msg;
    NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
    [outputStream write:[data bytes] maxLength:[data length]];
}


@end
