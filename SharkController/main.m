//
//  main.m
//  SharkController
//
//  Created by Peter Marton on 1/18/13.
//  Copyright (c) 2013 Peter Marton. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PHAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PHAppDelegate class]));
    }
}
