//
//  PHViewController.m
//  SharkController
//
//  Created by Peter Marton on 1/18/13.
//  Copyright (c) 2013 Peter Marton. All rights reserved.
//

#import "PHViewController.h"

@interface PHViewController () {
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
    
    BOOL isAutoForward;
}

@end

@implementation PHViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self initNetworkCommunication];
    
    isAutoForward = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// initNetworkCommunication
- (void) initNetworkCommunication {
	
	CFReadStreamRef readStream;
	CFWriteStreamRef writeStream;
	CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"10.0.2.20", 6767, &readStream, &writeStream);
	
	inputStream = (NSInputStream *)CFBridgingRelease(readStream);
	outputStream = (NSOutputStream *)CFBridgingRelease(writeStream);
	[inputStream setDelegate:(id)self];
	[outputStream setDelegate:(id)self];
	[inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[inputStream open];
	[outputStream open];
	
}

// sendMessage
- (void)sendMessage:(NSString*) msg {
    NSString *response  = [NSString stringWithFormat:@"%@", msg];
    NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
    [outputStream write:[data bytes] maxLength:[data length]];
}

# pragma mark button events

- (IBAction)autoForwardButton:(id)sender {
    isAutoForward = !isAutoForward;
    
    if(isAutoForward == YES) {
        [self sendMessage:@"AFON"];
        _autoForwardButton.backgroundColor = [UIColor redColor];
    } else {
        [self sendMessage:@"AFOFF"];
        _autoForwardButton.backgroundColor = [UIColor whiteColor];
    }
}

- (IBAction)forwardButton:(id)sender {
    [self sendMessage:@"F"];
}

- (IBAction)danceButton:(id)sender {
    [self sendMessage:@"DANCE"];
}

- (IBAction)handleRightButton:(id)sender {
    [self sendMessage:@"R"];
}

- (IBAction)handleLeftButton:(id)sender {
    [self sendMessage:@"R"];
}
@end
