//
//  PHAngryViewController.h
//  SharkController
//
//  Created by Peter Marton on 1/19/13.
//  Copyright (c) 2013 Peter Marton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PHAngryViewController : UIViewController

- (IBAction)panGestureHandle:(UIPanGestureRecognizer *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *drawImage;
- (IBAction)stopButtonHandle:(id)sender;
- (IBAction)reconnectButtonHandle:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *ballView;
@end
