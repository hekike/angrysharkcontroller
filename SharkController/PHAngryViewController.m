//
//  PHAngryViewController.m
//  SharkController
//
//  Created by Peter Marton on 1/19/13.
//  Copyright (c) 2013 Peter Marton. All rights reserved.
//

#import "PHAngryViewController.h"

@interface PHAngryViewController () {
    CGPoint _startCenter;
    
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
    
    int _startTime;
}

@end

@implementation PHAngryViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self initNetworkCommunication];
    
}


- (IBAction)panGestureHandle:(UIPanGestureRecognizer *)gesture {
    if ((gesture.state == UIGestureRecognizerStateChanged) ||
        (gesture.state == UIGestureRecognizerStateEnded)) {
        
        CGPoint location = [gesture locationInView:[_ballView superview]];
       // location.y += 30;
        
        // move ball
        CGAffineTransform transform = CGAffineTransformMakeTranslation(location.x - _startCenter.x, location.y - _startCenter.y);
        _ballView.transform = transform;
        
        
        // draw vector
        float endX =  _startCenter.x - (location.x - _startCenter.x);
        float endY =  _startCenter.y - (location.y - _startCenter.y);
        
        _drawImage.image = [UIImage imageNamed:@"44x44"];
        
        if(endY < _startCenter.y) {
            UIGraphicsBeginImageContext(_drawImage.frame.size);
            [_drawImage.image drawInRect:CGRectMake(0, 0, _drawImage.frame.size.width, _drawImage.frame.size.height)];
            CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
            CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 5.0);
            CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 1.0, 0.0, 0.0, 1.0);
            CGContextMoveToPoint(UIGraphicsGetCurrentContext(), location.x, location.y);
            CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), endX, endY);
            CGContextStrokePath(UIGraphicsGetCurrentContext());
            CGContextFlush(UIGraphicsGetCurrentContext());
            _drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        
        
        // reset ball
        if (gesture.state == UIGestureRecognizerStateEnded) {
            float s = sqrt(pow((location.x - _startCenter.x), 2) + pow((location.y - _startCenter.y), 2));
            // NSLog(@"vec length: %f", s);
            double lengthTime = CFAbsoluteTimeGetCurrent() - _startTime;
            
            //NSLog(@"time length: %f", lengthTime);
            
            float v = s / lengthTime;
            // NSLog(@"v: %f", v);
            
            float tanAlpha = (location.y - _startCenter.y) / (location.x - _startCenter.x);
            float angle = atan(tanAlpha);
            angle = angle * (180 / M_PI);
            
            if(angle < 0) {
                angle = 90 + angle;
            } else {
                angle =  -90 + angle;
            }
            
            // normalize s
            if(s > 200) {
                s = 200;
            }
            s = ceil(s / 20);
            
            NSLog(@"s: %f, v:%f, angle: %f", s, v, angle);
            [self sendMessage:[NSString stringWithFormat:@"G,%d,%d", (int)s, (int)angle]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                _drawImage.image = [UIImage imageNamed:@"44x44"];
            });
            
            [UIView animateWithDuration:0.3 animations:^{
                _ballView.transform = CGAffineTransformIdentity;
            }];
        }
        
    } else if(gesture.state == UIGestureRecognizerStateBegan) {
        _startCenter = _ballView.center;
        // _startCenter.y += 30;
        
        _startTime = CFAbsoluteTimeGetCurrent();
    }
}

# pragma mark communication

// initNetworkCommunication
- (void) initNetworkCommunication {
	
	CFReadStreamRef readStream;
	CFWriteStreamRef writeStream;
	CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"10.0.2.20", 6767, &readStream, &writeStream);
	
	inputStream = (NSInputStream *)CFBridgingRelease(readStream);
	outputStream = (NSOutputStream *)CFBridgingRelease(writeStream);
	[inputStream setDelegate:(id)self];
	[outputStream setDelegate:(id)self];
	[inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[inputStream open];
	[outputStream open];
    
}

// sendMessage
- (void)sendMessage:(NSString*) msg {
    
    @try {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, (unsigned long)NULL), ^(void) {
            NSData *data = [[NSData alloc] initWithData:[msg dataUsingEncoding:NSASCIIStringEncoding]];
            [outputStream write:[data bytes] maxLength:[data length]];
        });
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
    
}


- (IBAction)stopButtonHandle:(id)sender {
    [self sendMessage:@"S"];
}

- (IBAction)reconnectButtonHandle:(id)sender {
    [self initNetworkCommunication];
}
@end
