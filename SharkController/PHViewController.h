//
//  PHViewController.h
//  SharkController
//
//  Created by Peter Marton on 1/18/13.
//  Copyright (c) 2013 Peter Marton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PHViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *autoForwardButton;

- (IBAction)autoForwardButton:(id)sender;
- (IBAction)forwardButton:(id)sender;
- (IBAction)danceButton:(id)sender;

- (IBAction)handleRightButton:(id)sender;
- (IBAction)handleLeftButton:(id)sender;
@end
